import os
from flask import Flask, request, render_template, make_response, Blueprint, g, redirect, url_for
from datetime import datetime

app = Flask(
    __name__,
    static_url_path="",
    static_folder="web"
)


@app.route("/")
def home():
    return app.send_static_file("KolnHome.html")


@app.route("/cathedral")
def cathedral():
    return app.send_static_file("KolnCathedral.html")


@app.route("/transport")
def transport():
    return app.send_static_file("KolnTransport.html")


@app.route("/reviews", methods=["POST", "GET"])
def reviews():
    if request.method == "POST":
        name = request.form["name"]
        message = request.form["message"]

        if len(name) == 0 or len(message) == 0:
            return app.send_static_file("KolnReviews.html")
        else:
            date = datetime.now()

            with open('web/reviews.json', 'r') as f:
                lines = f.readlines()

                l1 = lines.pop()
                l2 = lines.pop()
                l3 = lines.pop()

                if l3 == '        }\n':
                    new = [
                        '        },\n',
                        '        {\n',
                        '            "name": "' + name + '",\n',
                        '            "message": "' + message + '",\n',
                        '            "date": "' +
                        date.strftime("%d/%m/%Y") + '",\n',
                        '            "time": "' +
                        date.strftime("%H:%M:%S") + '"\n',
                        '        }\n',
                        '    ]\n',
                        '}\n'
                    ]
                else:
                    new = [
                        '    "reviews": [\n',
                        '        {\n',
                        '            "name": "' + name + '",\n',
                        '            "message": "' + message + '",\n',
                        '            "date": "' +
                        date.strftime("%d/%m/%Y") + '",\n',
                        '            "time": "' +
                        date.strftime("%H:%M:%S") + '"\n',
                        '        }\n',
                        '    ]\n',
                        '}\n'
                    ]

                lines.extend(new)

            with open('web/reviews.json', 'w') as f:
                f.writelines(lines)

            return app.send_static_file("KolnReviews.html")
    else:
        return app.send_static_file("KolnReviews.html")


# @app.route("/<usr>")
# def user(usr):
#     return f"<h1>{usr}</h1>"
